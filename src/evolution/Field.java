package evolution;

import java.util.*;


/**
 * Rectangular map with rolled sides on which all the evolution takes place
 */
public class Field implements IPositionChangeObserver, IWorldMap {
    private final int width;
    private final int height;
    private final Vector2d upperRightOut;
    private Random generator = new Random();

    private List<Animal> animals;
    private Map<Vector2d, LinkedList<Animal>> animalsMap;
    private Map<Vector2d, Grass> steps = new HashMap<>();
    private Map<Vector2d, Grass> jungle = new HashMap<>();


    /** Make a field with given attributes */
    public Field(int width, int height, double jungleRatio) {
        this.width = width;
        this.height = height;
        this.upperRightOut = new Vector2d(width, height);

        this.animals = new LinkedList<>();
        this.animalsMap = new HashMap<>();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                animalsMap.put(new Vector2d(i, j), new LinkedList<>());

        Vector2d lowerLeftJungle = new Vector2d((int) (width/2.*(1.-jungleRatio)), (int) (height/2.*(1.-jungleRatio)));
        Vector2d upperRightJungle = new Vector2d((int) (width/2.*(1.+jungleRatio)), (int) (height/2.*(1.+jungleRatio)));
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++) {
                Vector2d v = new Vector2d(i, j);
                if (v.precedes(upperRightJungle) && v.follows(lowerLeftJungle))
                    jungle.put(v, null);
                else
                    steps.put(v, null);
            }
    }


    /** Place N grasses on the steps an N grasses in the jungle at the beginning */
    public void place2NGrasses(int n) {
        for (int i = 0; i < n; i++)
            grassGrow();
    }
    /** Place N pioneers to start the whole play */
    public void placeNAnimals(int n) {
        List<Vector2d> freePositions = new ArrayList<>(animalsMap.keySet());
        for (int i = 0; i < n; i++) {
            Vector2d position = freePositions.get(generator.nextInt(freePositions.size()));
            Animal animal = new Animal(this, Settings.START_ENERGY, position);
            place(animal);
            freePositions.remove(position);
        }
    }


    /**
     * Place an animal on it's position or one of the neighbour cells if the former is occupied
     */
    @Override
    public void place(Animal animal) {
        Vector2d position = animal.getPosition();
        if (!canMoveTo(position)) {
            for (int i = 1; i < MapDirection.directionsNumber; i++) {
                MapDirection nextToOrientation = animal.getOrientation().rotateClockwise(i);
                Vector2d nextTo = position.add(nextToOrientation.toUnitVector()).roll(upperRightOut);
                if (canMoveTo(nextTo)) {
                    animal.rotate(i);
                    animal.step();
                    animals.add(animal);
                    animalsMap.get(animal.getPosition()).add(animal);
                    animal.rotate(generator.nextInt(MapDirection.directionsNumber));
                    animal.addObserver(this);
                    return;
                }
            }

            animal.rotate(generator.nextInt(MapDirection.directionsNumber));
            animal.step();
        }
        animals.add(animal);
        animalsMap.get(animal.getPosition()).add(animal);
        animal.addObserver(this);
    }

    @Override
    public void positionChanged(Animal animal, Vector2d oldPosition, Vector2d newPosition) {
        this.animalsMap.get(oldPosition).remove(animal);
        this.animalsMap.get(newPosition).add(animal);
    }

    /** Remove all starving animals out of map */
    private void cleanDead() {
        animals.removeIf(Animal::isStarving);
        for (LinkedList<Animal> animalList : animalsMap.values())
            animalList.removeIf(Animal::isStarving);
    }
    /** Make all animals rotate and take a step */
    private void rotateNStep() {
        for (Animal animal : animals) {
            animal.step();
        }
    }
    /** Take grasses and feed all the animals on cells containing plants */
    private void feedingTime() {
        for (Vector2d v : animalsMap.keySet()) {
            if (animalsMap.get(v).isEmpty())
                continue;
            int food = pickGrass(v);
            if (food == 0)
                continue;
            int maxEnergy = -Settings.START_ENERGY;
            for (Animal animal : animalsMap.get(v))
                maxEnergy = Math.max(maxEnergy, animal.getEnergy());
            List<Animal> strongestAnimals = new LinkedList<>();
            for (Animal animal : animalsMap.get(v))
                if (animal.getEnergy() == maxEnergy)
                    strongestAnimals.add(animal);

            int foodPortion = food / strongestAnimals.size();
            for (Animal animal: strongestAnimals)
                animal.eat(foodPortion);
        }
    }
    /** Make all animals strong enough and together reproduce */
    private void breeding() {
        for (LinkedList<Animal> animalList : animalsMap.values()) {
            if (animalList.size() < 2)
                continue;
            animalList.sort((a, b) -> Integer.compare(b.getEnergy(), a.getEnergy()));
            if (animalList.get(1).getEnergy() >= Settings.BREEDING_ENERGY) {
                Animal child = animalList.get(0).crossover(animalList.get(1));
                place(child);
            }
        }
    }
    /** Plant one grass on the step and one in the jungle */
    private void grassGrow() {
        List<Vector2d> freeStepPositions = new LinkedList<>();
        for (Vector2d v : steps.keySet())
            if (steps.get(v) == null && animalsMap.get(v).isEmpty())
                freeStepPositions.add(v);
        List<Vector2d> freeJunglePositions = new LinkedList<>();
        for (Vector2d v : jungle.keySet())
            if (jungle.get(v) == null && animalsMap.get(v).isEmpty())
                freeJunglePositions.add(v);

        if (!freeStepPositions.isEmpty()) {
            Vector2d placeForPlant = freeStepPositions.get(generator.nextInt(freeStepPositions.size()));
            steps.replace(placeForPlant, new Grass());
        }
        if (!freeJunglePositions.isEmpty()) {
            Vector2d placeForPlant = freeJunglePositions.get(generator.nextInt(freeJunglePositions.size()));
            jungle.replace(placeForPlant, new Grass());
        }
    }

    /** Run one day of evolution using methods from above */
    @Override
    public void run() {
        cleanDead();
        rotateNStep();
        feedingTime();
        breeding();
        grassGrow();
    }

    /** Return true if `position` is not occupied by an animal */
    @Override
    public boolean canMoveTo(Vector2d position) {
        Object object = objectAt(position);
        return object == null || object instanceof Grass;
    }

    /** Return object which is occupying `position` (if both then animal) */
    @Override
    public Object objectAt(Vector2d position) {
        if (!animalsMap.get(position).isEmpty())
            return animalsMap.get(position).get(0);
        Grass grass = steps.getOrDefault(position, null);
        if (grass == null)
            grass = jungle.getOrDefault(position, null);
        return grass;
    }


    /** Return amount of energy harvested from the given position */
    public int pickGrass(Vector2d position) {
        Map<Vector2d, Grass> area;
        if (steps.containsKey(position))
            area = steps;
        else
            area = jungle;
        if (area.getOrDefault(position, null) != null) {
            area.replace(position, null);
            return Settings.PLANT_ENERGY;
        }
        return 0;
    }

    /** Return position with coordinates bounding whole map */
    @Override
    public Vector2d getBoundingCorner() {
        return upperRightOut;
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
}
