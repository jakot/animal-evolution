package evolution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Class representing the 8-direction movement schema
 */
public enum MapDirection {
    NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST;
    private final static List<MapDirection> indexedDirections = new ArrayList<>(Arrays.asList(MapDirection.values()));
    public final static int directionsNumber = indexedDirections.size();

    public String toString() {
        switch (this) {
            case EAST: return "East";
            case WEST: return "West";
            case NORTH: return "North";
            case SOUTH: return "South";
            case NORTH_EAST: return "Northern east";
            case NORTH_WEST: return "Northern west";
            case SOUTH_EAST: return "Southern east";
            case SOUTH_WEST: return "Southern west";
        }
        throw new IllegalStateException("Wrong direction.");
    }

    /** Return direction made by rotating given one `i` times step */
    public MapDirection rotateClockwise(int i) {
        return indexedDirections.get((this.ordinal() + i) % directionsNumber);
    }

    /** Return unit vector representation of the direction */
    public Vector2d toUnitVector() {
        switch (this) {
            case NORTH: return  new Vector2d(0, 1);
            case NORTH_EAST: return  new Vector2d(1, 1);
            case EAST: return  new Vector2d(1, 0);
            case SOUTH_EAST: return  new Vector2d(1, -1);
            case SOUTH: return  new Vector2d(0, -1);
            case SOUTH_WEST: return  new Vector2d(-1, -1);
            case WEST: return  new Vector2d(-1, 0);
            case NORTH_WEST: return  new Vector2d(-1, 1);
        }
        throw new IllegalStateException("Wrong direction.");
    }
}
