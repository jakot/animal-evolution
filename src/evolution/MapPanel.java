package evolution;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Implements panel to visualize every frame of animation
 */
public class MapPanel extends JPanel implements ActionListener {
    private Field field;


    /** Make panel for visualize given field */
    public MapPanel(Field field) {
        this.field = field;
        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(Settings.CELL_SIZE*field.getWidth(), Settings.CELL_SIZE*field.getHeight()));

        Timer timer = new Timer(Settings.DELAY, this);
        timer.start();
    }


    /** Draw single frame of animation */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        for (int i = 0; i < field.getWidth(); i++)
            for (int j = 0; j < field.getHeight(); j++) {
                Vector2d position = new Vector2d(i, j);
                if (field.objectAt(position) instanceof Animal) {
                    g2d.setColor(Color.YELLOW);
                    g2d.fillRect(Settings.CELL_SIZE*i+1, Settings.CELL_SIZE*j+1,
                            Settings.CELL_SIZE-2, Settings.CELL_SIZE-2);
                }
                else if (field.objectAt(position) instanceof Grass) {
                    g2d.setColor(Color.GREEN);
                    g2d.fillOval(Settings.CELL_SIZE * i+1, Settings.CELL_SIZE * j+1,
                            Settings.CELL_SIZE - 2, Settings.CELL_SIZE - 2);
                }
            }
    }

    /** Perform running evolution and repainting */
    @Override
    public void actionPerformed(ActionEvent e) {
        field.run();
        repaint();
    }
}
