package evolution;


import java.awt.*;


/**
 * Read file "parameters.json" and run evolution with provided settings
 */
public class World {
    public static void main(String[] args) {
        Settings.readFile("parameters.json");
        Field field = new Field(Settings.WIDTH, Settings.HEIGHT, Settings.JUNGLE_RATIO);
        field.place2NGrasses(Settings.INITIAL_GRASSES/2);
        field.placeNAnimals(Settings.INITIAL_ANIMALS);
        EventQueue.invokeLater(() -> {
            MapSwingTimer timer = new MapSwingTimer(field);
            timer.setVisible(true);
        });
    }
}
