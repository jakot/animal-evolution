package evolution;

import java.util.*;

/**
 * Class implementing animals' genetics
 */
public class Brain {
    private List<Integer> genotype;
    private static Random generator = new Random();


    /**
     * Make a brain with random, but right genotype
     */
    public Brain() {
        genotype = new ArrayList<>();
        for (int i = 0; i < 32; i++)
            genotype.add(generator.nextInt(8));
        balanceGenes();
    }


    /** Make gene of any type appear at least once and sort genotype */
    public void balanceGenes() {
        Set<Integer> allGenes = new HashSet<>();
        for (int i = 0; i < 8; i++)
            allGenes.add(i);
        while (!genotype.containsAll(allGenes)) {
            for (int i = 0; i < 8; i++)
                if (!genotype.contains(i)) {
                    int index = generator.nextInt(8);
                    genotype.set(index, i);
                }
        }
        genotype.sort(Integer::compare);
    }

    /** Return random gene */
    public int randomGene() {
        return genotype.get(generator.nextInt(32));
    }

    /** Create a combination of two brains */
    public Brain combineTwo(Brain other) {
        int index1 = generator.nextInt(32);
        int index2 = generator.nextInt(32);
        if (index2 < index1) {
            int tmp = index1;
            index1 = index2;
            index2 = tmp;
        }

        Brain newOne = new Brain();
        for (int i = 0; i < index1; i++)
            newOne.genotype.set(i, this.genotype.get(i));
        for (int i = index1; i < index2; i++)
            newOne.genotype.set(i, other.genotype.get(i));
        for (int i = index2; i < 32; i++)
            newOne.genotype.set(i, this.genotype.get(i));

        newOne.balanceGenes();
        return newOne;
    }
}
