package evolution;

import javax.swing.*;


/**
 * Timer class for implementing animation
 */
public class MapSwingTimer extends JFrame {
    public MapSwingTimer(Field field) {
        JPanel panel = new MapPanel(field);

        add(panel);

        setResizable(false);
        pack();

        setTitle("Animation");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
