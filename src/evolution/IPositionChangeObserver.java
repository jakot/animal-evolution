package evolution;


/**
 * Interface giving base for implementing observer pattern
 */
public interface IPositionChangeObserver {
    void positionChanged(Animal animal, Vector2d oldPosition, Vector2d newPosition);
}
