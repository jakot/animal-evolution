package evolution;

import java.util.ArrayList;
import java.util.Random;

/**
 * Virtual creature wandering in random directions on a map
 */
public class Animal {
    private MapDirection orientation;
    private ArrayList<IPositionChangeObserver> observers = new ArrayList<>();
    private Vector2d position;
    private IWorldMap map;
    private static Random generator = new Random();

    private Brain brain;
    private int energy;

    /** Make new random animal */
    public Animal(IWorldMap map, int energy, Vector2d initialPosition) {
        this.map = map;
        this.position = initialPosition;
        this.orientation = MapDirection.NORTH.rotateClockwise(generator.nextInt(8));
        this.brain = new Brain();
        this.energy = energy;
    }
    /** Make an animal with given brain */
    public Animal(IWorldMap map, int energy, Vector2d initialPosition, Brain brain) {
        this.map = map;
        this.position = initialPosition;
        this.orientation = MapDirection.NORTH.rotateClockwise(generator.nextInt(8));
        this.brain = brain;
        this.energy = energy;
    }


    public String toString() {
        switch (this.orientation) {
            case NORTH: return "^";
            case NORTH_EAST: return "┐";
            case EAST: return ">";
            case SOUTH_EAST: return "┘";
            case SOUTH: return "v";
            case SOUTH_WEST: return "└";
            case WEST: return "<";
            case NORTH_WEST: return "┌";
        }
        throw new IllegalStateException("Wrong orientation on map.");
    }

    /** Rotate animal clockwise `i` steps */
    public void rotate(int i) {
        this.orientation = this.orientation.rotateClockwise(i);
    }

    /** Make animal do one step on a map using portion of its energy*/
    public void step() {
        consumeEnergy(Settings.MOVE_ENERGY);
        useBrain();
        Vector2d newPosition = this.position.add(this.orientation.toUnitVector());
        newPosition = newPosition.roll(this.map.getBoundingCorner());
        this.positionChanged(this.position, newPosition);
        this.position = newPosition;
    }

    void addObserver(IPositionChangeObserver observer) {
        observers.add(observer);
    }

    public MapDirection getOrientation() {
        return this.orientation;
    }
    public Vector2d getPosition() {
        return this.position;
    }
    public int getEnergy() {
        return this.energy;
    }

    void positionChanged(Vector2d oldPosition, Vector2d newPosition) {
        for (IPositionChangeObserver observer : observers)
            observer.positionChanged(this, oldPosition, newPosition);
    }

    public void eat(int food) {
        this.energy += food;
    }
    public void consumeEnergy(int energy) {
        this.energy -= energy;
    }
    /** Return true if animal has too low energy */
    public boolean isStarving() {
        return this.energy < 1;
    }
    /** Choose random direction based on current direction and genotype */
    public void useBrain() {
        this.rotate(brain.randomGene());
    }
    /** Create an offspring with the other parent */
    public Animal crossover(Animal other) {
        int firstParentEnergy = this.energy/4;
        this.energy -= firstParentEnergy;
        int secondParentEnergy = other.energy/4;
        other.energy -= secondParentEnergy;
        int offspringEnergy = firstParentEnergy + secondParentEnergy;
        return new Animal(this.map, offspringEnergy, this.position, this.brain.combineTwo(other.brain));
    }
}
