package evolution;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;


/**
 * Class containing static settings for whole evolution read from json file
 */
public class Settings {
    public static int WIDTH = 60;
    public static int HEIGHT = 30;
    public static int START_ENERGY = 30;
    public static int MOVE_ENERGY = 1;
    public static int PLANT_ENERGY = 30;
    public static double JUNGLE_RATIO = 0.15;
    public static int BREEDING_ENERGY = START_ENERGY/2;

    public static int INITIAL_ANIMALS = 240;
    public static int INITIAL_GRASSES = 70;

    public static int CELL_SIZE = 20;
    public static int DELAY = 80;


    /** Read and set fields from file */
    public static void readFile(String filename) {
        JSONParser jsonParser = new JSONParser();
        try(Reader reader = new FileReader(filename)) {
            JSONObject jo = (JSONObject) jsonParser.parse(reader);

            WIDTH = (int) (long) jo.get("width");
            HEIGHT = (int) (long) jo.get("height");
            START_ENERGY = (int) (long) jo.get("startEnergy");
            MOVE_ENERGY = (int) (long) jo.get("moveEnergy");
            PLANT_ENERGY = (int) (long) jo.get("plantEnergy");
            JUNGLE_RATIO = (double) jo.get("jungleRatio");

            INITIAL_ANIMALS = (int) (long) jo.get("initialAnimals");
            INITIAL_GRASSES = (int) (long) jo.get("initialGrasses");

            CELL_SIZE = (int) (long) jo.get("cellSize");
            DELAY = (int) (long) jo.get("delay");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
