package evolution;


/**
 * Implementation of 2-dimensional coordinates used to describe position of every element on a map
 */
public class Vector2d {
    public final int x;
    public final int y;

    /** Make vector with giver coordinates */
    public Vector2d(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public String toString() {
        return String.format("(%d,%d)", this.x, this.y);
    }

    /** Return true if both coordinates of given vector are less that the other one's */
    public boolean precedes(Vector2d other) {
        return this.x <= other.x && this.y <= other.y;
    }
    /** Opposite of the above */
    public boolean follows(Vector2d other) {
        return this.x >= other.x && this.y >= other.y;
    }

    /** Add two vectors in algebraic sense */
    public Vector2d add(Vector2d other) {
        int x = this.x + other.x;
        int y = this.y + other.y;
        return new Vector2d(x, y);
    }

    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Vector2d))
            return false;
        Vector2d that = (Vector2d) other;
        return this.x == that.x && this.y == that.y;
    }

    /** Adjust coordinates so the vector points a position in rectangle between (0,0) and `other` */
    public Vector2d roll(Vector2d other) {
        int x = (this.x + other.x)%other.x;
        int y = (this.y + other.y)%other.y;
        return new Vector2d(x, y);
    }

    @Override
    public int hashCode() {
        int hash = 13;
        hash += this.x * 21;
        hash += this.y * 37;
        return hash;
    }
}
