package evolution;


/**
 * Class representing a grass on a map
 */
public class Grass {

    public Grass() {
    }

    public String toString() {
        return "*";
    }
}
