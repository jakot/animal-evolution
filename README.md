# Animal Evolution

### author: J. Kot

## Java project demonstrating OOP skills.

![Meadow](meadow.png)

The programme generates a grid on which animals (yellow boxes) are added.
Then the evolution starts.

In every step every animal can eat a plant (green circle) it is standing on.
Animals are so smart they go in random directions based on their genotypes.
Every step an animal consumes portion of its energy when making a move.
The energy can be restored by eating plants.
Genotypes are random as well, but if two animals meet and give an offspring,
it inherits mixture of ancestors' genotypes.

Settings for the programme can be adjusted in *parameters.json* file.
Animation is done using **swing** and **awt**.
